<?php	
require_once("includes/initialize.php");

//session_start(); 
include_once("includes/config.php");

if(isset($_GET["logout"]) && $_GET["logout"]==1)
{

//	$url = 'https://www.facebook.com/logout.php?next=' . YOUR_SITE_URL .
//  '&access_token='.$token;
	//$facebook->destroySession();

	//Al presionar el boton logout, destruye todas las variables de session.
	session_destroy();
	header('Location: '.$return_url);	

}	
?>
<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <link href="#" rel="shortcut icon">

  <title>CONSIGNAR.net</title>
  <!-- Bootstrap CSS -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <!-- Estilos personalizados para estas plantillas -->
  <link href="jumbotron.css" rel="stylesheet">
  <link href="css/errores.css" rel="stylesheet">
   
   <!-- Se agrega la biblioteca de jquery y enseguida nuestro js de funciones-->
    <script type="text/javascript" src="js/jquery-1.10.2.min.js" ></script>
    <script type="text/javascript" src="js/validar.js" ></script>
	
	<!-- Libreria de Facebook -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<?php
if (logged_in()) {
?>
           <script type="text/javascript">
                    window.location = "inicio.php";
            </script>
    <?php
}

?>  
</head>
<?php
if (isset($_POST['btnlogin'])) {
      
    $email = trim($_POST['log_email']);
    $upass = trim($_POST['log_pword']);
    
    $h_upass = sha1($upass);
    //comprueba si el correo y contraseña es igual a nada o nulo a continuación se mostrará cuadro de mensaje.
    if ($email == '') {
?>    <script type="text/javascript">
                alert("Usuario o Contraseña invalida!.");
                </script>
            <?php
        
    } elseif ($upass == '') {
?>    <script type="text/javascript">
                alert("Usuario o Contraseña invalida!.");
                </script>
            <?php
    } else {
		//Se crea un nuevo objeto member
        $member = new member();
		//hace uso de la funcion estatica y pasamos los parametros.
		$res = $member::AuthenticateMember($email, $h_upass);
		//comprueba si la funcion retorna verdadero
		if($res == true){
			?>   <script type="text/javascript">
					//verdadero, redirige a inicio.php
					window.location = "inicio.php";
				</script>
			<?php
		
		
		} else {
?>    <script type="text/javascript">
                alert("Usuario o Contraseña invalida!.");
                window.location = "inicio.php";
                </script>
        <?php
        }
        
    }
} else {
    
    $email = "";
    $upass = "";
    
}

?>
<body>


  <div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type=
        "button"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class=
        "icon-bar"></span></button> 
         <a class="navbar-brand" href="inicio.php"><img  src="img/blanco.png" class="img-responsive"/></a>
		  
      </div>

      <div class="navbar-collapse collapse">
	  
        <form class="navbar-form navbar-right" method="POST" action="index.php">
            <div class="form-group">
              <input type="text" placeholder="Email" class="form-control" name="log_email">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Contraseña" class="form-control" name="log_pword">
            </div>
			<div class="form-group">
            <button type="submit" class="btn btn-success btn-sm" name="btnlogin">Ingresar</button>
           </div>
           <!--<div class="form-group">
           <input type="image" src="img/Ingresar1.png" value="Ingresar" />-->
           <div class="form-group">
			<fb:login-button size="large" perms="email,user_birthday"></fb:login-button>
           
</div>
<!--<div> <a href="fpass.php">Olvidaste tu contrase&ntilde;a ? </a></div>-->
<script>
$(function() {
  $.ajax({
    url: '//connect.facebook.net/es_ES/all.js',
    dataType: 'script',
    cache: true,
    success: function() {
      FB.init({
        appId: '306686212861902',
        xfbml: true
      });
      FB.Event.subscribe('auth.authResponseChange', function(response) {
        if (response && response.status == 'connected') {
          FB.api('/me', function(data) {
           // alert('Nombre: ' + data.email);
	
			window.location = "loginface.php?log_email="+data.name+"&log_pword="+data.id;
          });
        }
      });
    }
  });
});



// jquery extend function


</script>
			  
            
        </form>
		
      </div><!--/.navbar-collapse -->
	  
    </div>
	
  </div>


 <div class="container">
      <div class="rows">
        <div class="col-xs-6">
        <h3></h3>
        <br></br>
                <br></br>
        <p>&nbsp;</p>
        <p><img src="img/consignarNet2.jpg" width="550px"></p>
        </div>

        <div class="col-xs-6">
		 <form  action="register.php" class="form-horizontal" id="register" method="post"> 
            <fieldset>
              <legend></legend>
              <legend>Registrate</legend>
              <h4></h4>

              <div class="rows">
                <div class="col-xs-12">
                  <div class="form-group">
                    <div class="rows">
                      <div class="col-xs-12">
                        <div class="col-xs-6" id="divfname">
                          <input class="form-control input-lg" id="fName" name="fName" placeholder="Nombre" type="text">
						  <div id="mensaje1" class="errores"> Ingresa solo caracteres</div>
                        </div>

                        <div class="col-xs-6">
                          <input class="form-control input-lg" id="lName" name="lName" placeholder="Apellido" type="text" >
						  <div id="mensaje2" class="errores"> Ingresa solo caracteres</div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group" id="divemail">
                 	<div class="rows">
                      <div class="col-xs-12">
                        <div class="col-xs-12">
							<input class="form-control input-lg" id="email" name="email" placeholder="Correo Eléctronico" type="text" >
                            <div id="mensaje3" class="errores"> Correo inválido. Utilice un formato xxx@mail.com</div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group" id="divremail">
                    <div class="rows">
                      <div class="col-xs-12">
                        <div class="col-xs-12">
                              <input class="form-control input-lg" id="reemail" name="reemail" placeholder="Vuelva a ingresar el correo electrónico" type="text" >
                              <div id="mensaje4" class="errores"> Los correos no coinciden</div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group" id="divpass">
                    <div class="rows">
                      <div class="col-xs-12">
                        <div class="col-xs-12">
                          <input class="form-control input-lg" id="password" name="password" placeholder="Contraseña nueva" type="password" >
                          <div id="mensaje5" class="errores">Ingrese una contraseña</div>
                        </div>
                      </div>
                    </div>
                  </div>

                  
                  
				   <div class="form-inline">
                    <div class="rows">
                      <div class="col-lg-12">
						<p>  Al hacer clic en Registrarse, aceptas las Condiciones y confirmas que leíste nuestra Política de uso de datos, incluido el uso de cookies.</p>
					  </div>
					 </div>
				  </div>					 
                  <div class="form-group">
                    <div class="rows">
                      <div class="col-xs-8">
                        <div class="col-xs-12">
                          <!--<button class="btn btn-success btn-lg" type="submit" name="Submit" id="boton">Registrarse</button>-->
                          <input type="image" "submit" name"submit" id="boton" src="img/Registrarse1.png" value="Siguiente" />
                        </div>
                      </div>
                    </div>
                  </div>
				  					
				  
				</div>
              </div>
            </fieldset>
			
          </form>
        </div>
      </div><!--rows-->
    </div><!--container-->

  <hr>

  <footer>
    <p style="text-align: center">© Consignar 2014</p>
  </footer><!-- /container -->
  
  <!-- Bootstrap JavaScript -->
  <!-- Ponemos los js al final del documento para que las paginas carguen más rápido -->
  <script src="js/jquery.js"></script> 
  <script src="js/bootstrap.min.js"></script>

</body>
</html>