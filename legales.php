<?php	
require_once("includes/initialize.php");	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="">
    
<title>CONSIGNAR.net</title>

	<!-- Bootstrap CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Estilos personalizados para estas plantillas -->
    <link href="jumbotron.css" rel="stylesheet">

<style type="text/css">
body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,textarea,p,blockquote,th,td{margin:0;padding:0;}table{border-collapse:collapse;border-spacing:0;}fieldset,img{border:0;}address,caption,dfn,th,var{font-style:normal;font-weight:normal;}li{list-style:none;}caption,th{text-align:left;}h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}

body{
margin:0;
padding:0;
background: transparent;
font-family: Helvetica Neue, Helvetica, Arial;
color: #000000;
}

#wrapper{
margin: 0 auto;
width: 940px;
}

.main_content, .uppersection{
width: 940px;
float: left;
}

.logo{
margin: 0 auto 60px auto;
width: 160px;
text-align: center;
}

.logo h1{
width: 95px;
height: 70px;
margin: 44px auto 5px auto;
border-radius: 50%;
background: #fff;
color: #ffffff;
text-align: center;
padding-top: 25px;
font-size: 49px;
}

.logo span{
font-size: 22px;
text-align: center;
margin: 0 auto;
}

h2{
font-weight: normal;
text-align: center;
display: block;
font-size: 30px;
}

h2 strong{
text-transform: uppercase;
font-size: 64px;
}


h2 span{
font-style: italic;
font-size: 30px;
}

p{
font-size: 19px;
text-align: center;
padding: 20px 140px 0;
line-height: 26px;
font-weight: 300;
}
</style>

<?php 
	    //confirmacion de login
		confirm_logged_in();
	?> 
    
    <script type="text/javascript">
function isCheck() {
if (document.getElementById( 'tyc' ).checked == true) {
document.getElementById( 'boton' ).disabled = false;
} else {
document.getElementById( 'boton' ).disabled = true;
}
} 
</script>	
</head>
<body>

<div class="container">
		<div class="well">
		  <div class="row">
          <div align="center"><legend5>USUARIO</legend5></div>
				<div class="col-xs-6 col-md-2">
						<a data-target="#myModal" data-toggle="modal" href="" title=
						"Click aqui para Cambiar la Imagen.">
				<?php
				
				$mydb->setQuery("SELECT * FROM foto WHERE `member_id`='{$_SESSION['member_id']}'");
				$cur = $mydb->loadResultList();
				if ($mydb->affected_rows()== 0){
					echo '<img src="./uploads/p.jpg" class="img-thumbnail" width="200px" height="100px" />';	
				
				} 
				foreach($cur as $object){
				   
						echo '<img src="./uploads/'. $object->filename.'" class="img-thumbnail" width="200px" height="100px" />';
					
					}	
				?> 
					</a>	
					
				<!-- Modal -->
					<div class="modal fade" id="myModal" tabindex="-1">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button class="close" data-dismiss="modal" type=
									"button">×</button>

									<h4 class="modal-title" id="myModalLabel">Elija su foto de Perfil.</h4>
								</div>

								<form action="save_photo.php" enctype="multipart/form-data" method=
								"post">
									<div class="modal-body">
										<div class="form-group">
											<div class="rows">
												<div class="col-md-12">
													<div class="rows">
														<div class="col-md-8">
															<input name="MAX_FILE_SIZE" type=
															"hidden" value="1000000"> <input id=
															"upload_file" name="upload_file" type=
															"file">
														</div>

														<div class="col-md-4"></div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="modal-footer">
										<button class="btn btn-default" data-dismiss="modal" type=
										"button">Cerrar</button> <button class="btn btn-primary"
										name="savephoto" type="submit">Guardar Foto</button>
									</div>
								</form>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->
				</div>

			  <div class="col-xs-12 col-sm-6 col-md-10">


				  <ul class="nav nav-tabs">
					  <li>
						  <a href="perfil33.php">Datos Personales</a>
					  </li>

					  <li>
						  <a href="empresa.php">Datos Empresa</a>
					  </li>

					  <li>
						  <a href="referencia.php">Ref. Comerciales</a>
					  </li>
                        
				     <li>
						  <a href="banco.php">Datos Bancarios</a>
				    </li>
 
                 
                      <li class="active">
						  <a href="#">Terminos Legales</a>
					  </span></li>
                        
                </ul>  
                
                <form  action="register_legales.php" class="form-horizontal" id="register_legales" method="post">                 

<div class="tab-content">     
<br>
<div class="row">
<div class="col-xs-12">
<legend>
			      TERMINOS LEGALES
				  </legend>
                  <br>
                <span class="legales">QUIENES SOMOS.</span> 
                <br>
                Consignar.net es una firma Nacional etc etc.
<p></p>
<span class="legales">TERMINOS Y CONDICIONES PARA LA UTILIZACIÓN DEL SERVICIO WEB</span>
<p></p>
<span class="legales">GENERAL</span>
<br>
Lea los términos y condiciones generales (en adelante, “términos y condiciones”) que se describen a continuación. Estos serán aplicables para la navegación en el presente sitio de internet www.consignar.net (en adelante “el sitio web”) y los servicios que en éste se ofrecen. Los derechos de uso de la página consignar.net, son de propiedad de Consignar S.A.<p></p>
Los presentes términos y condiciones tienen carácter obligatorio y vinculante. Se aplican a todas las operaciones realizadas mediante el sitio web. La utilización del servicio web implica el conocimiento y aceptación de ellos. Si usted no está de acuerdo con los términos y condiciones, deberá abstenerse de utilizar el sitio web y/o los servicios por él ofrecidos. Por usuario del sitio web se entiende a toda aquella persona que se encuentre registrada.<p></p>
Los presentes términos y condiciones podrán ser modificados, debiendo los ingresantes al sitio web tener siempre los mismos presentes al momento de realizar una operación. No obstante ello, y para el supuesto de modificaciones, el sitio web pondrá un aviso en su página principal por un lapso de 5 días, alertando sobre la modificación.<p></p>
Todos los derechos del presente Sitio Web están reservados y corresponden a Consignar.net.<p></p>
<p></p>
<span class="legales">PRIVACIDAD DE DATOS PERSONALES</span>
<br>
Para poder utilizar el sitio web de manera eficiente y segura, los usuarios deberán aportar determinados datos personales esenciales para operar en el sitio. Los datos recabados serán incorporados a una base general de Usuarios de Consignar S.A.. La información ingresada será tratada de manera estrictamente confidencial, procurando Consignar S.A., llevar a cabo todas las medidas correspondientes para proteger la privacidad de los mismos, todo ello de acuerdo a lo normado por la ley 25.326. No obstante lo anterior, el usuario deberá tener en cuenta que internet no es un medio inexpugnable en cuanto a su seguridad. Los usuarios tienen la facultad de ejercer el derecho de acceso, rectificación, bloqueo o supresión de los datos personales  que hayan sido suministrados al sitio web personalmente o a través de un tercero.<p></p> 
Los datos consignados para la acreditación en el presente sitio web se encuentran resguardado y bajo responsabilidad de consignar.net de no ser difundidos por ningún medio y ninguna persona, salvo los supuestos de compra venta que se lleven a cabo.<p></p>
<span class="legales">REGISTRACIÓN</span>
<br>
Al momento de registración en este sitio web el interesado deberá proporcionar los datos personales que allí le sean requeridos, debiendo aceptar, expresamente los términos y condiciones que aquí se describen. <p></p>
Con la registración del Usuario el sitio web le proporcionará una clave de identificación personal, que le será requerida al momento de formalizar las operaciones que lleve a cabo.<p></p>
<span class="legales">CONDICIONES DE COMPRA VENTA</span>
<br>

<span class="legales2">Hacienda</span><br>
La hacienda que se ofrezca en el sitio web será publicada conjuntamente con el precio y plazo previamente acordado con el vendedor. Consignar.net garantizará que los valores publicados sean los efectivamente acordados, como así también la veracidad de las tropas exhibidas en el sitio web.<p></p>
La hacienda consignada deberá estar perfectamente identificada de manera visible y en un todo de acuerdo con la normativa vigente de SENASA y/o los demás organismos públicos.<p></p>
Los lotes consignados deberán conformar como mínimo una jaula de traslado completa, pudiendo ser las mismas simples o dobles.<p></p>
<span class="legales2">Exclusividad</span><br>
Los vendedores que publiquen su hacienda en el sitio web se comprometen a mantenerla a disposición exclusivamente de consignar.net por un período no menor a 72 hs. hábiles a fin de que este realice la venta.<p></p>
Caducado el plazo descripto sin que se haya materializado la venta, el vendedor puede libremente disponer de la hacienda ofrecida, manteniendo la misma en oferta o desistiendo de la venta.<p></p>
No obstante ello, las ofertas recibidas que sean consideradas razonables y dentro de los precios de mercado, serán transmitidas inmediatamente al vendedor.<p></p> 
<span class="legales2">Acuerdo</span><br>
Si se llegase a un acuerdo de compra, las partes deberán ratificar su intención a través de un contrato en el que se especifiquen los términos y condiciones de la operación, para validar la operación le será requerida su clave de identificación personal mencionada en el apartado correspondiente a registración.<p></p>
El contrato establecerá las condiciones inherentes a: cantidad, precio, forma de pago, plazos, comisiones, facturación, etc.-<p></p>
<span class="legales2">Compra</span><br>
Una vez realizada la compra habiendo aceptado las condiciones ofrecidas, ambas partes, comprador y vendedor, se llevarán a cabo las siguientes operaciones:<p></p>
Lo concerniente al recibo, pesada y posterior carga de la tropa estará a cargo de: (definir)<p></p>
La pesada definitiva se llevara a cabo de acuerdo a lo pactado con el vendedor y se le aplicará a la tropa el desbaste acordado. Los gastos de balanza, en caso utilizarse balanza pública y/o de Sociedad Rural y/o Ferias, serán a cargo del vendedor y los importes resultantes serán debitados de la cuenta de venta que le corresponde el mismo.<p></p> 
Todos los gastos sanitarios que se originen estarán a cargo del vendedor.<p></p>
Los demás gastos concernientes en: gastos de guías, certificados y demás documentación relativa a la carga de la hacienda serán soportados en partes iguales por el vendedor y el comprador, salvo que acuerdo en contrario. En este caso los importes resultantes serán debitados de las liquidaciones correspondientes a cada uno.<p></p>
Se pactará entre las partes las condiciones para la carga de la tropa, de no poderse llevar a cabo por causas ajenas a la voluntad de las partes, se decidirá un nuevo cronograma de cargas definido por el comprador y el vendedor.<p></p>
En caso que la operación de compra-venta sea pactada con aplicación del sistema que relaciona peso del animal con precio, conocido en nuestro negocio con el nombre de "la tablita" se considerará lo siguiente: se efectuará un descuento de $0,01 sobre kilo vivo por cada segmento de 1 kilos vivos por animal que superen los kilos promedio por animal pactados. En caso que la operación de compra-venta se haya acordado con un tope máximo de kilos vivos, los kilos excedentes se consideran por muertos (no se abonarán bajo ningún concepto). A los efectos de aplicar los cálculos descriptos en el párrafo anterior se tomarán en cuenta los kilos netos resultantes (con el desbaste ya realizado) sobre el promedio general de la tropa, no pudiendo aplicarse sobre pesadas parciales EN CASO QUE SE ACUERDE LO CONTRARIO EN LA NEGOCIACION ENTRE LAS PARTES.<p></p>
Los gastos de fletes estarán a cargo del COMPRADOR salvo que se indique lo contrario en la publicación.<p></p>
<span class="legales2">Plazos</span><br>
El plazo acordado para efectuar el pago por parte del comprador comenzará a regir a partir de la carga de la hacienda, debiendo consignar.net abonar el importe correspondiente al vendedor al quinto día hábil posterior a la fecha de haber realizado el cobro.<p></p>
<span class="legales2">Comisiones</span><br>
El VENDEDOR abonará el X,X % en concepto de comisión el cual le será debitado de la cuenta de la venta.<p></p>
El COMPRADOR abonará el X,X% en concepto de comisión que se le adicionará en la cuenta de compra.<p></p>
Las retenciones y/o percepciones impositivas que correspondan se realizarán de acuerdo a las normativas vigentes al momento de la facturación de la operación y al momento de la cancelación de las mismas, agregándose las sumas resultantes a los débitos y/o adiciones calculados en los dos párrafos anteriores.<p></p>
<span class="legales2">Facturación</span><br>
Consignar.net será responsable de la facturación, liquidación y cobranza de las operaciones concretadas, asumiendo en consecuencias las obligaciones inherentes a las mismas.<p></p>
<span class="legales">JURISDICCIÓN Y LEGISLACIÓN APLICABLE</span><br>
<p></p>
Los presentes Términos y Condiciones se encuentran regidos sin excepción y en todos sus puntos por las leyes de la República Argentina y serán interpretados de acuerdo a ellas.<p></p>
Ante cualquier diferencia, desacuerdo o conflicto derivado de la interpretación, validez, alcance y/o aplicación de los presentes Términos y Condiciones generales, los usuarios se comunicarán con CONSIGNAR.NET de manera fehaciente, haciéndole llegar su reclamo, para que las partes traten de arribar a un acuerdo.<p></p>
En caso de que no sea posible arribar a una solución, y para garantizar a los consumidores el pleno acceso a la justicia, los usuarios podrán elegir y someter su reclamo a una de las siguientes opciones e instancias:<p></p>
1.	Sistema nacional de arbitraje de consumo del ministerio de economía y producción de la nación. los procedimientos ante este sistema son gratuitos, y no es necesario contar con patrocinio letrado. Asimismo, se garantiza el equilibrio entre las partes y la transparencia del proceso, y los laudos emitidos por dicho tribunal tienen autoridad de cosa juzgada y son irrecurribles (para saber más sobre este sistema ingresar en http://www.mecon.gov.ar/snac)<p></p>
2.	Dirección general de defensa del consumidor del gobierno de La Pampa<p></p>
3.	Tribunales ordinarios de la Primera Circunscripción Judicial de la Provincia de La Pampa con competencia en la materia.<p></p>
Una vez elegida una de las opciones, se excluyen las demás alternativas.<p></p>Para el caso de que cualquiera de las opciones anteriores fuesen gravosas o económicamente inviables para usuarios domiciliados en el exterior, las partes, a pedido del usuario, determinarán de común acuerdo un mecanismo mutuamente conveniente para resolver sus diferencias.

<p></p>
 <div class="pull-right">
				<legend2>
<label class="checkbox-inline">
 	
He leido y acepto las condiciones <input type="checkbox" id="tyc" name="tyc" onchange="isCheck()"> 
  </label>

</legend2>
				</div> 
</div>

				<p></p>	
				<p></p>
                <p></p>
                 <div class="pull-right">
				<!--<button type="submit" class="btn btn-primary" id="boton" name="boton"  Disabled>Siguiente</button>-->
                <input type="image" id="boton" name="boton" src="img/Guardar1.png" value="Guardar" Disabled/>
                <!--<button type="submit" class="btn btn-primary" enabled>Cancelar</button>--> 
                <input type="image" src="img/Cancelar1.png" value="Cancelar" enabled/>
			  </div>   
			  
              
</div> 
			  </form>
			
</body>
</html>
