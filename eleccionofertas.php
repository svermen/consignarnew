<?php	
require_once("includes/initialize.php");	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CONSIGNAR.net</title>

	<!-- Bootstrap CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Estilos personalizados para estas plantillas -->
    <link href="jumbotron.css" rel="stylesheet">
<style>
table {
  border-collapse: separate;
  border-spacing: 0 5px;
}

thead th {
  background-color: #006DCC;
  color: white;
}

tbody td {
  background-color: #EEEEEE;
}

tr td:first-child,
tr th:first-child {
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
}

tr td:last-child,
tr th:last-child {
  border-top-right-radius: 6px;
  border-bottom-right-radius: 6px;
}
	</style>
<?php 
	    //confirmacion de login
		confirm_logged_in();
	?> 	
</head>


<body>
<div class="container">
<div class="row">
<div class="col-xs-5" id='principal' >
</div>

<div  class="col-xs-5" id='principal2' >
</div>

</div>	
</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

     <script src="assets/js/tooltip.js"></script>
	<script src="assets/js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script>
	
    $( '#principal' ).load( 'verofertas.php');
	$( '#principal2' ).load( 'verofertasrealizadas.php');
</script>
</body>
</html>
