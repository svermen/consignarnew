<?php	
require_once("includes/initialize.php");	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="">

    <title>CONSIGNAR.net</title>
    
	<!-- Bootstrap CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Estilos personalizados para estas plantillas -->
    <link href="jumbotron.css" rel="stylesheet">

<style>
table {
  border-collapse: separate;
  border-spacing: 0 5px;
}

thead th {
  background-color: #006DCC;
  color: white;
}

tbody td {
  background-color: #EEEEEE;
}

tr td:first-child,
tr th:first-child {
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
}

tr td:last-child,
tr th:last-child {
  border-top-right-radius: 6px;
  border-bottom-right-radius: 6px;
}
body{
	margin:0;
padding:0;
background: transparent;
}
	</style>
<?php 
	    //confirmacion de login
		confirm_logged_in();
	?> 	
</head>

<body>
<div class="well">
		  <div class="row">
          
		<legend>Ofertas Recibidas</legend>
		  <div class="container">
		 <input class="form-control input-lg" id="member" name="member" value="<?php echo $_SESSION['member_id']?>" type="hidden">
				
						
				<?php
				
				$mydb->setQuery("SELECT * 
FROM ofertas 
inner join ventas on ventas.Id_ventas = ofertas.id_ventas 
inner join ventas_datos on ventas_datos.id_ventas = ofertas.id_ventas
inner join categoria on ventas_datos.categoria = categoria.id_categoria 
inner join raza on ventas_datos.raza = raza.id_raza 
where `id_vendedor`='{$_SESSION['member_id']}'"); 
				$cur = $mydb->loadResultList();
				if ($mydb->affected_rows()== 0){
						
				
				} 
				foreach($cur as $object){
				   
		?>	
        
        <div class="row">
        
        <div class="col-xs-4">
        <br>
        <img src="img/oferta1.jpg" height="120" width="120" alt="" class="img-rounded" />
       <br><br>
       </div>
   
     <div class="col-xs-5">
	 <br><br>
            Raza: <?php echo $object->descripcion_raza; ?> <br />
        Tipo: <?php echo $object->descripcion_categoria; ?><br />
		Cantidad: <?php echo $object->cantidad; ?><br />
           </div>
          <div class="col-xs-1">
		  <br><br>
        <a href="ofertas_recibidas.php?ID=<?php echo $object->id_ventas;?>&ID2=<?php echo $object->id_oferta;?>"><img src="img/ver.png" height="60" width="60" alt="Click para ver la oferta" /></a>
          </div>
        <legend></legend>
		</div>
<?php			
					}	
					
				?> 

</div>
</div>
</div>
       
        
        			

	
</div>
	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
     <script src="assets/js/tooltip.js"></script>
	<script src="assets/js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
