<?php	
require_once("includes/initialize.php");	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CONSIGNAR.net</title>

	<!-- Bootstrap CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Estilos personalizados para estas plantillas -->
    <link href="jumbotron.css" rel="stylesheet">
<style>
table {
  border-collapse: separate;
  border-spacing: 0 5px;
}

thead th {
  background-color: #006DCC;
  color: white;
}

tbody td {
  background-color: #EEEEEE;
}

tr td:first-child,
tr th:first-child {
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
}

tr td:last-child,
tr th:last-child {
  border-top-right-radius: 6px;
  border-bottom-right-radius: 6px;
}
	</style>

<style type="text/css">
body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,textarea,p,blockquote,th,td{margin:0;padding:0;}table{border-collapse:collapse;border-spacing:0;}fieldset,img{border:0;}address,caption,dfn,th,var{font-style:normal;font-weight:normal;}li{list-style:none;}caption,th{text-align:left;}h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}

body{
margin:0;
padding:0;
background: #ffffff;
font-family: Helvetica Neue, Helvetica, Arial;
color: #000000;
}

#wrapper{
margin: 0 auto;
width: 940px;
}

.main_content, .uppersection{
width: 940px;
float: left;
}

.logo{
margin: 0 auto 60px auto;
width: 160px;
text-align: center;
}

.logo h1{
width: 95px;
height: 70px;
margin: 44px auto 5px auto;
border-radius: 50%;
background: #fff;
color: #ffffff;
text-align: center;
padding-top: 25px;
font-size: 49px;
}

.logo span{
font-size: 22px;
text-align: center;
margin: 0 auto;
}

h2{
font-weight: normal;
text-align: center;
display: block;
font-size: 30px;
}

h2 strong{
text-transform: uppercase;
font-size: 64px;
}


h2 span{
font-style: italic;
font-size: 30px;
}

p{
font-size: 19px;
text-align: center;
padding: 20px 140px 0;
line-height: 26px;
font-weight: 300;
}

</style>
<!-- Bootstrap CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Estilos personalizados para estas plantillas -->
    <link href="jumbotron.css" rel="stylesheet">

<?php 
	    //confirmacion de login
		confirm_logged_in();
	?> 	
</head>
<body>
<br><br><br>


<div class="container">
<br><br><br><br><br><br>
		
		  <h2 class="titulo_publicacion">85 Terneros Pampa del Infierno (Chaco)</h2>
  <script type="text/javascript">
    jQuery(document).ready(function(){
        document.title = "85 Terneros Pampa del Infierno (Chaco)";
    });
</script>
    <div id="contenedor_left" style="margin-top:20px;">
      
            <div id="multimedia">
            <div id="m_barra">
                <a href="#" onclick="lote_media('v',this); return false;" class="selected">
                    Ver Video<img src="/imagenes/bar/video.png" height="20"></a>
                <a href="#" onclick="lote_media('m',this); return false;">
                    Ver Ubicación<img src="/imagenes/bar/mapa.png" height="20"></a>
                <a href="#" onclick="lote_media('f',this); return false;">
                    Ver Fotos<img src="/imagenes/bar/foto.png" height="20"></a>
            </div>
            <div class="varios">

                <div class="carousel" id="video">
                    <div id="control_videos">
                                <a href="javascript:" class="carousel-control next" style="position: absolute !important; margin-top: -20em !important; display: none;" rel="next">Siguiente</a>
                            <a href="javascript:" class="carousel-control prev" style="position: absolute !important; margin-top: -20em !important; display: none;" rel="prev">Anterior</a>
                                                </div>
                    <div class="middle" id="video_middle">
                        <div class="inner" id="video_inner">
                        <li id="video_li_1"><embed type="application/x-shockwave-flash" src="/p/pl.swf" style="undefined" id="video_span_1" name="video_span_1" quality="high" allowfullscreen="true" allowscriptaccess="always" wmode="opaque" flashvars="skin=/p/bekle.swf&amp;lightcolor=3678B4&amp;autostart=false&amp;controlbar=over&amp;stretching=fill&amp;file=../videos/i20/ce906acb9e8c1761b987f3070c90db1dcd185594.flv" height="320" width="430"></li></div>
                    </div>
                </div>

                <div class="carousel" id="fotos_lote" style="display: none;">
                    <div id="control_fotos">
                        <a href="javascript:" class="carousel-control next" rel="next">Siguiente</a>
                        <a href="javascript:" class="carousel-control prev" rel="prev">Anterior</a>
                    </div>
                    <div class="middle" style="height: auto !important;">
                        <div class="inner">
    <img src="/videos/i20/ce906acb9e8c1761b987f3070c90db1dcd185594_01.jpg" alt="" height="320px" width="430px"><img src="/videos/i20/ce906acb9e8c1761b987f3070c90db1dcd185594_02.jpg" alt="" height="320px" width="430px"><img src="/videos/i20/ce906acb9e8c1761b987f3070c90db1dcd185594_05.jpg" alt="" height="320px" width="430px"><img src="/videos/i20/ce906acb9e8c1761b987f3070c90db1dcd185594_06.jpg" alt="" height="320px" width="430px">                        </div>
                    </div>
                </div>
                                            <div id="mapa_lote" class="carousel" style="display: none;">
                     <img src="http://maps.google.com/staticmap?markers=-26.511752649239487,-61.1722964803954,red&amp;key=ABQIAAAAuZl2RqCyDcmpQH7nomSBfhT46WDffqrPaaCPJBvK66lNUtKDxRSuS8Lf6OB0g1FaiApzbAwJASAUAA&amp;zoom=5&amp;size=440x330" alt="ubicacion" id="mapa_lote_img">
                </div>
            </div>
        </div>



        


        
        <!-- cambios MN -->
    <div class="c_new_box" style="float:none; margin-top:10px;">

        
        <div class="precio_ofertar" id="cambio">

                <form action="" method="get" id="formc" onsubmit="posteoAjax('/__dcac/application/obsoletos_dcac/ver_lote/ver_lote.php ',this, 'cambio'); return false;">
        <input value="3969" name="rev" style="float:left; margin:0 20px 0 5px;" type="hidden">
        <div class="tit_ingrese_oferta" style="width:300px;">Ingrese su oferta </div>
         
         
         <div class="ingrese_monto" style="float:left;margin-top:5px;margin-left:10px;">
        
         <span class="signo_pesos">$</span>
         <span class="contenedor-controles-input-selector"></span><input min="0" max="30000" name="precio" class="less_input" id="precio" readonly="readonly" rel="selector[0,30000]" value="22.00" type="text"><a class="aumentar-input-selector" style="cursor: pointer;" id="precio_AUMENTAR"><img src="/imagenes/botones/sp.gif" alt="Aumentar" title="Aumentar"></a><a class="disminuir-input-selector" style="cursor: pointer;" id="precio_DISMINUIR"><img src="/imagenes/botones/sp.gif" alt="Disminuir" title="Disminur"></a> <span class="kg_iva"> / Kg + IVA</span> 
         <div class="mas_menos" style=""></div><br>
         <center><div class="" style="font-size:11px; float:left;color:#acacac;margin-top:12px;margin-left:10px;  ">Oferta valida por los proximos<br> 60 minutos</div></center>
                    
       
          
         </div>
         
        <div style="float:left; margin-left:20px;"> <b style="width: 100px; float: left; margin-top:12px;"> Plazo 
             </b>
            
            <input name="directo" value="" type="hidden"><br>
           <input value="30 " name="plazo" id="input_plazo" type="text">
              <span style="font-size:11px; position: absolute;  margin-left:34px; color:#858383; ">(días)</span> 
           </div>
        
        <div style="float:left; margin-left:20px; margin-top:-3px;"> <b style="width: 100px; float: left; "> Comentario 
                 </b>
            
            <input name="directo" value="" type="hidden"><br>
          <textarea name="comentario_publicacion" id="comentario_publicacion" style=";"></textarea>
           </div>
         
         <div class="d_new_col_1" style="margin:10px 0 0 10px; width:400px; margin-top:20px; display:inline-block;">
         <input name="directo" value="" type="hidden">
         <!-- <input type="text" value=""  name="plazo" class="margin-left-50" />-->
   
        </div>
         
   
         <div style="clear:both"></div>
         <div style="height:40px;padding-bottom:15px !important;">
         <p class="btn_oferta" style="margin-top:-15px;"> 
         <input name="oferta_pre" class="a_new_boton1 green" value="Enviar Oferta" id="mostrar" type="submit">
         </p>
         </div>
            
            <script type="text/javascript">
                window.onload = new function() {
                    modificar_input_selector(0.10);
}
</script>
        </form>   </div></div>
        
        
        
       
        
      
        
        
        
        
         <br>
         
        


        
                
    </div> <!-- cierro contenedor_left -->

    <div id="contenedor_right" style="margin-top:38px;">

        <div id="info_hacienda">

            <div id="titulo_right">
                <div class="texto">
                    
                        <div class="luz">Información del lote</div>
                    
                </div>
            </div>

            <div class="bloque_right">
                <dl class="right_ul info2">
                    <dt class="ee">Id Lote:</dt>
                    <dd class="ee"><strong>3969</strong></dd>

                    <dt>Ubicación:</dt>
                    <dd>Pampa del Infierno, Chaco</dd>

                    <dt class="ee">Categoria:</dt>
                    <dd class="ee">Terneros</dd>

                    <dt>Raza:</dt>
                    <dd>Brangus Negro</dd>
                            <dd>Brangus Colorado</dd>
                            <dd>Braford</dd>
                        

                    <dt class="ee">Peso Aprox.:</dt>
                    <dd class="ee"><strong>190</strong> Kgs.</dd>

                    <dt>Cantidad:</dt>
                    <dd><strong>85</strong> Cabezas</dd>

                    <dt class="ee">Descarte:</dt>
                    <dd class="ee"><strong>2</strong> Animales</dd>
                        <dt>Edad:</dt>
                    <dd><strong>10</strong> Meses</dd>

                    <dt class="ee">Trazada:</dt>
                    <dd class="ee"><img src="/imagenes/tick.png" alt=""></dd>

                    <dt>Marca liq.:</dt>
                    <dd><img src="/imagenes/tick.png" alt=""></dd>
                </dl>
            </div>
        </div>

        <div class="titulo_gris"><div>Informe de revisación</div></div>
            <div class="bloque_right">

            <dl class="right_ul info3">
                <dt>Est. Corporal:</dt>
                <dd><strong>4</strong></dd>
                <dt>Calidad:</dt>
                <dd><strong>8</strong></dd>
                <dt>Uniformidad:</dt>
                <dd>Parejo</dd>
                <dt>Sanidad:</dt>
                <dd>Muy buena</dd>
                <dt>Garrapata:</dt>
                <dd><img src="/imagenes/tick.png" alt=""></dd>
                <dt>Mio-Mio:</dt>
                <dd><img src="/imagenes/cross.png" alt=""></dd>
                <dt>Zona:</dt>
                <dd>Sucia</dd>
                        <dt>Saben Comer:</dt>
                    <dd><img src="/imagenes/tick.png" alt=""></dd>
                                        <dt>Destetados:</dt>
                    <dd><img src="/imagenes/tick.png" alt=""></dd>
                                            <dt>Fecha Destete:</dt>
                        <dd>0000-00-00</dd>
                                <dt>Capados:</dt>
                    <dd><img src="/imagenes/cross.png" alt=""></dd>
                    </dl>
                                
	
</div>
	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
     <script src="assets/js/tooltip.js"></script>
	<script src="assets/js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
