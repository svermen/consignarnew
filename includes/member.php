<?php

require_once(LIB_PATH.DS.'database.php');
class member {
	
	protected static $tbl_name = "usuarios";
	function db_fields(){
		global $mydb;
		return $mydb->getFieldsOnOneTable(self::$tbl_name);
	}
	function listOfmembers(){
		global $mydb;
		$mydb->setQuery("Select * from ".self::$tbl_name);
		$cur = $mydb->loadResultList();
		return $cur;
	
	}
	 static function AuthenticateMember($email="", $h_upass=""){
		global $mydb;
		$res=$mydb->setQuery("SELECT * FROM `usuarios` WHERE `email`='" . $email . "' and `pword`='" . $h_upass ."' LIMIT 1");
		 $found_user = $mydb->loadSingleResult();
		    $_SESSION['member_id'] = $found_user->member_id;
            $_SESSION['fName']     = $found_user->fName;
            $_SESSION['lName']     = $found_user->lName;
 $_SESSION['username']     = $found_user->member_id;            
$_SESSION['email']     = $found_user->email;
            $_SESSION['pword']     = $found_user->pword;
            //$_SESSION['mm']        = $found_user->mm;
            //$_SESSION['dd']        = $found_user->dd;
            //$_SESSION['yy']        = $found_user->yy;
            //$_SESSION['gender']    = $found_user->gender; -->
		return $found_user;	
	} 	

	static function bPrimary($id=0){
		global $mydb;
		$mydb->setQuery("SELECT * FROM ".self::$tbl_name." WHERE auto_id={$id} LIMIT 1");
		$row = $mydb->loadSingleResult();
		$s = $row->autostart + $row->incval;
		$a = $row->appenchar;
		return $a.$s;
	}	
	static function bPrimaryUpdate($id=0){
		global $mydb;
		$mydb->setQuery("SELECT * FROM ".self::$tbl_name." WHERE auto_id={$id} LIMIT 1");
		$row = $mydb->loadSingleResult();
		$s = $row->autostart + $row->incval;
			
		return $s;
	}		
	/*---Instanciaci�n de objetos de forma din�mica---*/
	static function instantiate($record) {
		$object = new self;

		foreach($record as $attribute=>$value){
		  if($object->has_attribute($attribute)) {
		    $object->$attribute = $value;
		  }
		} 
		return $object;
	}
	
	
	/*--Limpieza de los datos en bruto antes de enviar a la base de datos--*/
	private function has_attribute($attribute) {
	  // No nos importa acerca del valor, s�lo queremos saber si existe la clave
	  // Retorna verdadero o falso
	  return array_key_exists($attribute, $this->attributes());
	}

	protected function attributes() { 
		// devolver una matriz de nombres de atributos y sus valores
	  global $mydb;
	  $attributes = array();
	  foreach($this->db_fields() as $field) {
	    if(property_exists($this, $field)) {
			$attributes[$field] = $this->$field;
		}
	  }
	  return $attributes;
	}
	
	protected function sanitized_attributes() {
	  global $mydb;
	  $clean_attributes = array();
	  // sanear los valores antes de presentar
	  // Nota: no altera el valor real de cada atributo
	  foreach($this->attributes() as $key => $value){
	    $clean_attributes[$key] = $mydb->escape_value($value);
	  }
	  return $clean_attributes;
	}
	
	
	/*--Metodo de creacion, modificacion y borrado--*/
	public function save() {
	  // Un nuevo registro no tendr� un id todav�a.
	  return isset($this->id) ? $this->update() : $this->create();
	}
	
	public function create() {
		global $mydb;

		$attributes = $this->sanitized_attributes();
		$sql = "INSERT INTO ".self::$tbl_name." (";
		$sql .= join(", ", array_keys($attributes));
		$sql .= ") VALUES ('";
		$sql .= join("', '", array_values($attributes));
		$sql .= "')";
	echo $mydb->setQuery($sql);
	
	 if($mydb->executeQuery()) {
	    $this->id = $mydb->insert_id();
	    return true;
	  } else {
	    return false;
	  }
	}

	public function update($id=0) {
	  global $mydb;
		$attributes = $this->sanitized_attributes();
		$attribute_pairs = array();
		foreach($attributes as $key => $value) {
		  $attribute_pairs[] = "{$key}='{$value}'";
		}
		$sql = "UPDATE ".self::$tbl_name." SET ";
		$sql .= join(", ", $attribute_pairs);
		$sql .= " WHERE auto_id=". $id;
	  $mydb->setQuery($sql);
	 	if(!$mydb->executeQuery()) return false; 	
		
	}

	public function delete($id=0) {
		global $mydb;
		  $sql = "DELETE FROM ".self::$tbl_name;
		  $sql .= " WHERE auto_id=". $id;
		  $sql .= " LIMIT 1 ";
		  $mydb->setQuery($sql);
		  
			if(!$mydb->executeQuery()) return false; 	
	
	}
		
}
?>