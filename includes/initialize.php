<?php
//definimos los principales path
defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

defined('SITE_ROOT') ? null : define ('SITE_ROOT', $_SERVER['DOCUMENT_ROOT'].DS.'consignarnew');

defined('LIB_PATH') ? null : define ('LIB_PATH',SITE_ROOT.DS.'includes');

// cargamos primero el archivo config.php 
require_once(LIB_PATH.DS."config.php");
//carga de funciones basicas
require_once(LIB_PATH.DS."functions.php");
//carga la clase session
require_once(LIB_PATH.DS."session.php");

//Carga los objetos principales
require_once(LIB_PATH.DS."database.php");

?>