<?php
	//antes de que almacenamos la informaci�n de nuestros miembros, tenemos que empezar primero la sesi�n
	session_start();
	
	//crear una nueva funcion para chequear que la variable de sesion member_id esta seteada
	function logged_in() {
		return isset($_SESSION['member_id']);		              
	}
	//esta funci�n si miembro de sesi�n no se establece entonces ser� redirigido a index.php
	function confirm_logged_in() {
		if (!logged_in()) {?>
			<script type="text/javascript">
				window.location = "index.php";
			</script>

		<?php
		}
	}
?>
