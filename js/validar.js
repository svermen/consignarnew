//Creamos dos variables que tendrán las expresiones regulares a ser comprobadas
//Una para el correo y otra para verIficar solo letras
var expr = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
var expr1 = /^[a-zA-Z]*$/;

$(document).ready(function () {
	$("#mensaje1, #mensaje2, #mensaje3, #mensaje4, #mensaje5").hide();
	$("#boton").click(function (){ //función para el boton de enviar
		//recolectamos en variables, lo que tenga cada input.
		//Para mejor manipulación en los if's 
		var nombre = $("#fName").val();
		var apellidos = $("#lName").val();
		var correo = $("#email").val();
		var passw = $("#password").val();
		var recorreo = $("#reemail").val();
		
        
        //Secuencia de if's para verificar contenido de los inputs
        
        //Verifica que no este vacío y que sean letras
        if(nombre == "" || !expr1.test(nombre)){
			$("#mensaje1").fadeIn("slow"); //Muestra mensaje de error
            return false;				   // con false sale de la secuencia
        }
        else{
			$("#mensaje1").fadeOut();	//Si el anterior if cumple, se oculta el error
			
            if(apellidos == "" || !expr1.test(apellidos)){
				$("#mensaje2").fadeIn("slow");
                return false;
            }
            else{
				$("#mensaje2").fadeOut();
 
                if(correo == "" || !expr.test(correo)){
					$("#mensaje3").fadeIn("slow");
                    return false;
                }
                else{
					$("#mensaje3").fadeOut();
					
							
					if(correo != recorreo){
						$("#mensaje4").fadeIn("slow");
						return false;
					}
					else{
					$("#mensaje4").fadeOut();
					
					if(passw == "" || expr1.test(passw)){
					$("#mensaje5").fadeIn("slow");
                    return false;
                }
                else{
					$("#mensaje5").fadeOut();
				}
					}
				}					
			}
		}
		
    });//fin click
    

	$("#nombre, #apellidos").keyup(function(){
		if( $(this).val() != "" && expr1.test($(this).val())){
			$("#mensaje1, #mensaje2").fadeOut();
			return false;
		}
    });
  
  
    $("#correo").keyup(function(){
		if( $(this).val() != "" && expr.test($(this).val())){
			$("#mensaje3").fadeOut();
			return false;
		}
    });
    
	$("#password").keyup(function(){
		if( $(this).val() != "" && expr1.test($(this).val())){
			$("#mensaje5").fadeOut();
			return false;
		}
    });
    
    var valido=false;
    $("#recorreo").keyup(function(e) {
		var correo = $("#email").val();
		var re_correo=$("#reemail").val();
		
		if(correo != re_correo)
		{
			$("#reemail").css({"background":"#F22" }); //El input se pone rojo
			valido=true;
		}
		else if(correo == re_correo)
		{
			$("#reemail").css({"background":"#8F8"}); //El input se ponen verde
			$("#mensaje4").fadeOut();	
			valido=true;
		}
    });//fin keyup recorreo
	
	$("#password").keyup(function(){
		if( $(this).val() != "" && expr1.test($(this).val())){
			$("#mensaje5").fadeOut();
			return false;
		}
    });
	
    
});//fin ready
