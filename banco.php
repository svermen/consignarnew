<?php	
require_once("includes/initialize.php");	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="">
    
<title>CONSIGNAR.net</title>

	<!-- Bootstrap CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Estilos personalizados para estas plantillas -->
    <link href="jumbotron.css" rel="stylesheet">

<style type="text/css">
body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,textarea,p,blockquote,th,td{margin:0;padding:0;}table{border-collapse:collapse;border-spacing:0;}fieldset,img{border:0;}address,caption,dfn,th,var{font-style:normal;font-weight:normal;}li{list-style:none;}caption,th{text-align:left;}h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}

body{
margin:0;
padding:0;
background: transparent;
font-family: Helvetica Neue, Helvetica, Arial;
color: #000000;
}

#wrapper{
margin: 0 auto;
width: 940px;
}

.main_content, .uppersection{
width: 940px;
float: left;
}

.logo{
margin: 0 auto 60px auto;
width: 160px;
text-align: center;
}

.logo h1{
width: 95px;
height: 70px;
margin: 44px auto 5px auto;
border-radius: 50%;
background: #fff;
color: #ffffff;
text-align: center;
padding-top: 25px;
font-size: 49px;
}

.logo span{
font-size: 22px;
text-align: center;
margin: 0 auto;
}

h2{
font-weight: normal;
text-align: center;
display: block;
font-size: 30px;
}

h2 strong{
text-transform: uppercase;
font-size: 64px;
}


h2 span{
font-style: italic;
font-size: 30px;
}

p{
font-size: 19px;
text-align: center;
padding: 20px 140px 0;
line-height: 26px;
font-weight: 300;
}
</style>

<?php 
	    //confirmacion de login
		confirm_logged_in();
	?> 	
</head>
<body>

<div class="container">
		<div class="well">
		  <div class="row">
          <div align="center"><legend5>USUARIO</legend5></div>
				<div class="col-xs-6 col-md-2">
						<a data-target="#myModal" data-toggle="modal" href="" title=
						"Click aqui para Cambiar la Imagen.">
				<?php
				
				$mydb->setQuery("SELECT * FROM foto WHERE `member_id`='{$_SESSION['member_id']}'");
				$cur = $mydb->loadResultList();
				if ($mydb->affected_rows()== 0){
					echo '<img src="./uploads/p.jpg" class="img-thumbnail" width="200px" height="100px" />';	
				
				} 
				foreach($cur as $object){
				   
						echo '<img src="./uploads/'. $object->filename.'" class="img-thumbnail" width="200px" height="100px" />';
					
					}	
				?> 
					</a>	
					
				<!-- Modal -->
					<div class="modal fade" id="myModal" tabindex="-1">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button class="close" data-dismiss="modal" type=
									"button">×</button>

									<h4 class="modal-title" id="myModalLabel">Elija su foto de Perfil.</h4>
								</div>

								<form action="save_photo.php" enctype="multipart/form-data" method=
								"post">
									<div class="modal-body">
										<div class="form-group">
											<div class="rows">
												<div class="col-md-12">
													<div class="rows">
														<div class="col-md-8">
															<input name="MAX_FILE_SIZE" type=
															"hidden" value="1000000"> <input id=
															"upload_file" name="upload_file" type=
															"file">
														</div>

														<div class="col-md-4"></div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="modal-footer">
										<button class="btn btn-default" data-dismiss="modal" type=
										"button">Cerrar</button> <button class="btn btn-primary"
										name="savephoto" type="submit">Guardar Foto</button>
									</div>
								</form>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->
				</div>

			  <div class="col-xs-12 col-sm-6 col-md-10">
				  

				  <ul class="nav nav-tabs">
					  <li>
						  <a href="perfil33.php">Datos Personales</a>
					  </li>

					  <li>
						  <a href="empresa.php">Datos Empresa</a>
					  </li>

					  <li>
						  <a href="referencia.php">Ref. Comerciales</a>
					  </li>
                        
				     <li class="active">
						  <a href="#">Datos Bancarios</a>
				    </li>
                 
                      <li>
						  <a href="legales.php">Terminos Legales</a>
					  </li>
                        
                </ul>                  
                 <form  action="register_banco.php" class="form-horizontal" id="register_banco" method="post"> 
                 <?php				   				
					$sql = mysql_query("select * from banco where `member_id`='{$_SESSION['member_id']}'"); 
					$row = mysql_fetch_array($sql);				
                ?>

<div class="tab-content">               
			    <br>
<legend>
Banco 1°
</legend>
<div class="row">
<div class="col-xs-4">
		<legend>
			      Nombre de Banco:
				  <input class="form-control input-lg" id="descripcion" name="descripcion" value="<?php echo $row['descripcion']; ?>"  type="text">
	    </legend>
</div>
<div class="col-xs-4">
		<legend>
			      Sucursal:
				  <input class="form-control input-lg" id="sucursal" name="sucursal" value="<?php echo $row['sucursal']; ?>"  type="text">				 
	    </legend>
</div>
</div>

<div class="row">
<div class="col-xs-8">
<legend>
			      CBU:
				  <input class="form-control input-lg" id="cbu" name="cbu" value="<?php echo $row['cbu']; ?>"  type="text">
	    </legend>
</div>
</div>

<br>
<legend>
Banco 2°
</legend>
<div class="row">
<div class="col-xs-4">
		<legend>
			      Nombre de Banco:
				  <input class="form-control input-lg" id="descripcion2" name="descripcion2" value="<?php echo $row['descripcion2']; ?>"  type="text">
	    </legend>
</div>
<div class="col-xs-4">
		<legend>
			      Sucursal:
				  <input class="form-control input-lg" id="sucursal2" name="sucursal2" value="<?php echo $row['sucursal2']; ?>"  type="text">				 
	    </legend>
</div>
</div>

<div class="row">
<div class="col-xs-8">
<legend>
			      CBU:
				  <input class="form-control input-lg" id="cbu2" name="cbu2" value="<?php echo $row['cbu2']; ?>"  type="text">
	    </legend>
</div>
</div>

<br>
<legend>
Banco 3°
</legend>
<div class="row">
<div class="col-xs-4">
		<legend>
			      Nombre de Banco:
				  <input class="form-control input-lg" id="descripcion3" name="descripcion3" value="<?php echo $row['descripcion3']; ?>"  type="text">
	    </legend>
</div>
<div class="col-xs-4">
		<legend>
			      Sucursal:
				  <input class="form-control input-lg" id="sucursal3" name="sucursal3" value="<?php echo $row['sucursal3']; ?>"  type="text">				 
	    </legend>
</div>
</div>

<div class="row">
<div class="col-xs-8">
<legend>
			      CBU:
				  <input class="form-control input-lg" id="cbu3" name="cbu3" value="<?php echo $row['cbu3']; ?>"  type="text">
	    </legend>
</div>
</div>
				<p></p>	
				<p></p>
                <p></p>
                <div class="pull-right">
				<!--<button type="submit" class="btn btn-primary" enabled>Guardar</button> -->
                <input type="image" src="img/Guardar1.png" value="Guardar" /> 
<!--			    <button type="submit" class="btn btn-primary" enabled>Cancelar</button>--> 
 <input type="image" src="img/Cancelar1.png" value="Cancelar" />
			  </div>    
			  
              
</div> 
</form>
</body>
</html>
