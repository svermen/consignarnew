<?php
//require_once("includes/initialize.php");
include("db.php");

    // esta funciÃ³n se va a llamar al cargar el primer combo
    function obtenerTodosLosPaises() {
        $paises = array();
        $sql = "SELECT idprovincia, nombre 
                FROM provincia"; 

        $db = obtenerConexion();

        // obtenemos todos los paÃ­ses
        $result = ejecutarQuery($db, $sql);

        // creamos objetos de la clase paÃ­s y los agregamos al arreglo
        while($row = $result->fetch_assoc()){
            $row['nombre'] = mb_convert_encoding($row['nombre'], 'UTF-8', mysqli_character_set_name($db));          
            $pais = new pais($row['idprovincia'], $row['nombre']);
            array_push($paises, $pais);
        }

        cerrarConexion($db, $result);

        // devolvemos el arreglo
        return $paises;
    }

    class pais {
        public $id;
        public $nombre;

        function __construct($id, $nombre) {
            $this->id = $id;
            $this->nombre = $nombre;
        }
    }
?>