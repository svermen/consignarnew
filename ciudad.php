<?php
//require_once("includes/initialize.php");
include("db.php");

    // si se estÃ¡ enviando por POST el id del paÃ­s
    // significa que intentamos acceder desde jQuery
    if(isset($_POST['idPais'])) {
        $ciudades = array();
        $sql = "SELECT idciudad, nombre 
                FROM ciudad 
                WHERE provincia_id = '".$_POST['idPais']."'order by nombre"; 


        $db = obtenerConexion();
		

        // obtenemos todas las ciudades
        $result = ejecutarQuery($db, $sql);

        // creamos objetos de la clase ciudad y los agregamos al arreglo
        while($row = $result->fetch_assoc()){
            $row['nombre'] = mb_convert_encoding($row['nombre'], 'UTF-8', mysqli_character_set_name($db));
            $ciudad = new ciudad($row['idciudad'], $row['nombre']);
            array_push($ciudades, $ciudad);
        }

        cerrarConexion($db, $result);

        // devolvemos el arreglo de ciudades, en formato JSON
        echo json_encode($ciudades);
    }

    class ciudad {
        public $id;
        public $nombre;

        function __construct($id, $nombre) {
            $this->id = $id;
            $this->nombre = $nombre;
        }
    }
?>