<?php	
require_once("includes/initialize.php");	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="">

    <title>CONSIGNAR.net</title>
    
	<!-- Bootstrap CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Estilos personalizados para estas plantillas -->
    <link href="jumbotron.css" rel="stylesheet">

<style>
table {
  border-collapse: separate;
  border-spacing: 0 50px;
}

thead th {
  background-color: #006DCC;
  color: white;
}

tbody td {
  /*background-color: #EEEEEE;*/
  background: transparent;
}

tr td:first-child,
tr th:first-child {
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
}

tr td:last-child,
tr th:last-child {
  border-top-right-radius: 6px;
  border-bottom-right-radius: 6px;
}
body{
margin:0;
padding:0;
background: transparent;

}

    
	</style>
<?php 
	    //confirmacion de login
		confirm_logged_in();
	?> 	
</head>

<body>
<div class="container">
<div class="row">
<div class="col-md-5" id='principal' >
</div>
<div  class="col-md-5" id='principal2' >
</div>

</div>
</div>	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

     <script src="assets/js/tooltip.js"></script>
	<script src="assets/js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script>
	
    $( '#principal' ).load( 'verventas0.php');
	$( '#principal2' ).load( 'verventas1.php');
</script>
</body>
</html>
