<?php	
require_once("includes/initialize.php");	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="">
    
<title>CONSIGNAR.net</title>

	<!-- Bootstrap CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Estilos personalizados para estas plantillas -->
    <link href="jumbotron.css" rel="stylesheet">
    <link href="css/errores.css" rel="stylesheet">
    
    
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    
	<!--Para validar los textbox -->
 	<script type="text/javascript" src="js/validar_empresa.js" ></script>
    
    <!-- Para que este disabled el boton siguiente hasta que acepten los terminos -->
    <script type="text/javascript">
//Funcion check de Terminos legales	
function isCheck() {
if (document.getElementById( 'tyc' ).checked == true) {
document.getElementById( 'boton' ).disabled = false;
} else {
document.getElementById( 'boton' ).disabled = true;
}
} 
</script>

<style type="text/css">
body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,textarea,p,blockquote,th,td{margin:0;padding:0;}table{border-collapse:collapse;border-spacing:0;}fieldset,img{border:0;}address,caption,dfn,th,var{font-style:normal;font-weight:normal;}li{list-style:none;}caption,th{text-align:left;}h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}

body{
margin:0;
padding:0;
/*background: #ffffff;*/
background: transparent;
font-family: Helvetica Neue, Helvetica, Arial;
color: #000000;
}

#wrapper{
margin: 0 auto;
width: 940px;
}

.main_content, .uppersection{
width: 940px;
float: left;
}

.logo{
margin: 0 auto 60px auto;
width: 160px;
text-align: center;
}

.logo h1{
width: 95px;
height: 70px;
margin: 44px auto 5px auto;
border-radius: 50%;
background: #fff;
color: #ffffff;
text-align: center;
padding-top: 25px;
font-size: 49px;
}

.logo span{
font-size: 22px;
text-align: center;
margin: 0 auto;
}

h2{
font-weight: normal;
text-align: center;
display: block;
font-size: 30px;
}

h2 strong{
text-transform: uppercase;
font-size: 64px;
}


h2 span{
font-style: italic;
font-size: 30px;
}

p{
font-size: 19px;
text-align: center;
padding: 20px 140px 0;
line-height: 26px;
font-weight: 300;
}
</style>

<?php 
	    //confirmacion de login
		confirm_logged_in();
	?> 	
    
     
</head>
<body>

<div class="container">
				<div class="well">
		  <div class="row">
          <div align="center"><legend5>USUARIO</legend5></div>
				<div class="col-xs-6 col-md-2">
						<a data-target="#myModal" data-toggle="modal" href="" title=
						"Click aqui para Cambiar la Imagen.">
				<?php
				
				$mydb->setQuery("SELECT * FROM foto WHERE `member_id`='{$_SESSION['member_id']}'");
				$cur = $mydb->loadResultList();
				if ($mydb->affected_rows()== 0){
					echo '<img src="./uploads/p.jpg" class="img-thumbnail" width="200px" height="100px" />';	
				
				} 
				foreach($cur as $object){
				   
						echo '<img src="./uploads/'. $object->filename.'" class="img-thumbnail" width="200px" height="100px" />';
					
					}	
				?> 
					</a>	
					
				<!-- Modal -->
					<div class="modal fade" id="myModal" tabindex="-1">
						<div style="margin-top: 100px;" class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button class="close" data-dismiss="modal" type=
									"button">�</button>

									<h4 class="modal-title" id="myModalLabel">Elija su foto de Perfil.</h4>
								</div>

								<form action="save_photo.php" enctype="multipart/form-data" method=
								"post">
									<div class="modal-body">
										<div class="form-group">
											<div class="rows">
												<div class="col-md-12">
													<div class="rows">
														<div class="col-md-8">
															<input name="MAX_FILE_SIZE" type=
															"hidden" value="1000000"> <input id=
															"upload_file" name="upload_file" type=
															"file">
														</div>

														<div class="col-md-4"></div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="modal-footer">
										<button class="btn btn-default" data-dismiss="modal" type=
										"button">Cerrar</button> <button class="btn btn-primary"
										name="savephoto" type="submit">Guardar Foto</button>
									</div>
								</form>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->
				</div>
                

			  <div class="col-xs-12 col-sm-6 col-md-10">
				
				   <ul class="nav nav-tabs">
					  <li class="active">
						  <a href="#">Datos Personales</a>
					  </li>

					  <li>
						  <a href="empresa.php">Datos Empresa</a>
					  </li>

					  <li>
						  <a href="referencia.php">Ref. Comerciales</a>
					  </li>
                        
				     <li>
						  <a href="banco.php">Datos Bancarios</a>
				    </li>
                 
                      <li>
						  <a href="legales.php">Terminos Legales</a>
					  </li>
                        
                </ul>
              
			    <form  action="register_personal.php" class="form-horizontal" id="register_personal" method="post"> 
                 <?php				   				
					$sql = mysql_query("select * from perfil where `member_id`='{$_SESSION['member_id']}'"); 
					$row = mysql_fetch_array($sql);											
                ?>
                
<!--				<form  action="register.php" class="form-horizontal" id="register" method="post">--> 
                 <?php	
					$sql1 = mysql_query("select * from usuarios where `member_id`='{$_SESSION['member_id']}'"); 
					$row1 = mysql_fetch_array($sql1);	
				?>
                
<div class="tab-content">

<div class="row">
<div class="col-xs-5">
            <legend>
			      Nombre:
				  <input class="form-control input-lg" id="fName" name="fName" value="<?php echo $row1['fName']; ?>" type="text">
				  
	    </legend>
	  </div>
<div class="col-xs-5">
			    <legend>
			      Apellido:
				  <input class="form-control input-lg" id="lName" name="lName" value="<?php echo $row1['lName']; ?>" type="text">

			    </legend>
</div>
</div>

<div class="row">
<div class="col-xs-5">
			    <legend>
			      Correo: 
				  <input class="form-control input-lg" id="email" name="email" value="<?php echo $row1['email'];?>" type="text"> 
			    </legend>
</div> 
<div class="col-xs-5">
			    <legend>
			      Lugar de Nacimiento: 
				  <input class="form-control input-lg" id="lugarnac" name="lugarnac" value="<?php echo $row['lugarnac'];?>" type="text"> 
			    </legend>
</div>          
</div>     
			    
                
<div class="row">
<div class="col-xs-5">
 				<legend>
			      Direccion: 
				  <input class="form-control input-lg" id="direccion" name="direccion" value="<?php echo $row['direccion'];?>" type="text"> 
			    </legend>
</div>                


				<div class="col-xs-5">
			    <legend>
			      DNI:
				  <input class="form-control input-lg" id="dni" name="dni" value="<?php echo $row['dni'];?>" type="text">
			    </legend>
				</div>
	</div>
                
<div class="row">                
<div class="col-xs-5">
	            <legend>
			      Codigo Postal:
				  <input class="form-control input-lg" id="cp" name="cp" value="<?php echo $row['cp'];?>" type="text">
			    </legend>
	  </div>
                
				<div class="col-xs-5">
			    <legend>
			      Telefono:
				<input class="form-control input-lg" id="tel" name="tel" value="<?php echo $row['tel'];?>" type="text">
			    </legend>
	</div>
    </div>
   <div class="row">  
    <div class="col-xs-5">
    <legend>
			      Perfil de usuario:
				  <?php
     			//SELECCIONAMOS TODOS LOS DATOS DE LA TABLA 
                 $buscar = mysql_query("SELECT * FROM perfil_usuario"); 
                 $buscar2 = mysql_query("SELECT id_perfil_usuario FROM perfil where `member_id`='{$_SESSION['member_id']}'");
             
                 //OBTENEMOS LA CANTIDAD DE REGISTROS ENCONTRADOS 
                 $resultados = mysql_num_rows($buscar); 
 
				$perfilcond=mysql_fetch_array($buscar2);

                //SI LA CANTIDAD DE REGISTROS ENCONTRADOS ES MAYOR A CERO, ES DECIR, SI SE 	ENCONTRARON DATOS, EMPEZAMOS A IMPRIMIRLOS EN EL COMBOBOX 
                 if($resultados>0){ 
				 ?>
                 <!--CREAMOS EL COMBOBOX--> 
                 <select class="form-control input-lg" id="id_perfil_usuario" name="id_perfil_usuario" >
				<option 0>Seleccionar</option>
                 <?php 
                 //AL ARRAY $datos LE ASIGNAMOS TODOS LOS REGISTROS ENCONTRADOS EN LA TABLA 
                 while($datos = mysql_fetch_array($buscar)){ 
                 ?> 
                 
                 <option <?php if($datos["id_perfil_usuario"] == $perfilcond["id_perfil_usuario"]){echo("selected = selected");}?> value=<?=$datos["id_perfil_usuario"]?> > <?=$datos["descripcion"]?> </option> 
                 <?php 
                 } 
                 ?> 
                 <!--CERRAMOS EL COMBOBOX--> 
             </select> 
             
             <?php 
 } 
 ?> 
        </legend>
    </div>
    
    <div class="col-xs-5">
	            <legend id="otro">
			      Otro Perfil:
				  <input class="form-control input-lg" id="otroperfil" name="otroperfil" value="<?php echo $row['otroperfil'];?>" type="text">
			    </legend>
	  </div>
    
    </div>
	
 			
	             

		

  <legend4>Fecha de nacimiento</legend4>                           
                         
<div class="row"> 

<div class="col-xs-3">

<?php
     			//SELECCIONAMOS TODOS LOS DATOS DE LA TABLA 
                 $buscar = mysql_query("SELECT * FROM dia"); 
                 $buscar2 = mysql_query("SELECT id_dia FROM perfil where `member_id`='{$_SESSION['member_id']}'"); 
             
                 //OBTENEMOS LA CANTIDAD DE REGISTROS ENCONTRADOS 
                 $resultados = mysql_num_rows($buscar); 
 
				$diacond=mysql_fetch_array($buscar2);

                //SI LA CANTIDAD DE REGISTROS ENCONTRADOS ES MAYOR A CERO, ES DECIR, SI SE 	ENCONTRARON DATOS, EMPEZAMOS A IMPRIMIRLOS EN EL COMBOBOX 
                 if($resultados>0){ 
				 ?>
                 <!--CREAMOS EL COMBOBOX--> 
                 <select class="form-control input-lg" id="id_dia" name="id_dia" >
				<option 0>Dia</option>
                 <?php 
                 //AL ARRAY $datos LE ASIGNAMOS TODOS LOS REGISTROS ENCONTRADOS EN LA TABLA 
                 while($datos = mysql_fetch_array($buscar)){ 
                 ?> 
                 
                 <option <?php if($datos["id_dia"] == $diacond["id_dia"]){echo("selected = selected");}?> value=<?=$datos["id_dia"]?> > <?=$datos["desc_dia"]?> </option> 
                 <?php 
                 } 
                 ?> 
                 <!--CERRAMOS EL COMBOBOX--> 
             </select> 
             
             <?php 
 } 
 ?>
      </div>
<div class="col-xs-3">
                  
                                               <?php
     			//SELECCIONAMOS TODOS LOS DATOS DE LA TABLA 
                 $buscar = mysql_query("SELECT * FROM mes"); 
                 $buscar2 = mysql_query("SELECT id_mes FROM perfil where `member_id`='{$_SESSION['member_id']}'"); 
             
                 //OBTENEMOS LA CANTIDAD DE REGISTROS ENCONTRADOS 
                 $resultados = mysql_num_rows($buscar); 
 
				$mescond=mysql_fetch_array($buscar2);

                //SI LA CANTIDAD DE REGISTROS ENCONTRADOS ES MAYOR A CERO, ES DECIR, SI SE 	ENCONTRARON DATOS, EMPEZAMOS A IMPRIMIRLOS EN EL COMBOBOX 
                 if($resultados>0){ 
				 ?>
                 <!--CREAMOS EL COMBOBOX--> 
                 <select class="form-control input-lg" id="id_mes" name="id_mes" >
				<option 0>Mes</option>
                 <?php 
                 //AL ARRAY $datos LE ASIGNAMOS TODOS LOS REGISTROS ENCONTRADOS EN LA TABLA 
                 while($datos = mysql_fetch_array($buscar)){ 
                 ?> 
                 
                 <option <?php if($datos["id_mes"] == $mescond["id_mes"]){echo("selected = selected");}?> value=<?=$datos["id_mes"]?> > <?=$datos["desc_mes"]?> </option> 
                 <?php 
                 } 
                 ?> 
                 <!--CERRAMOS EL COMBOBOX--> 
             </select> 
             
             <?php 
 } 
 ?> 
                          
                          
      </div>
                        <div class="col-xs-4">
							
                          
                          <?php
     			//SELECCIONAMOS TODOS LOS DATOS DE LA TABLA 
                 $buscar = mysql_query("SELECT * FROM anio"); 
                 $buscar2 = mysql_query("SELECT id_anio FROM perfil where `member_id`='{$_SESSION['member_id']}'"); 
             
                 //OBTENEMOS LA CANTIDAD DE REGISTROS ENCONTRADOS 
                 $resultados = mysql_num_rows($buscar); 
 
				$aniocond=mysql_fetch_array($buscar2);

                //SI LA CANTIDAD DE REGISTROS ENCONTRADOS ES MAYOR A CERO, ES DECIR, SI SE 	ENCONTRARON DATOS, EMPEZAMOS A IMPRIMIRLOS EN EL COMBOBOX 
                 if($resultados>0){ 
				 ?>
                 <!--CREAMOS EL COMBOBOX--> 
                 <select class="form-control input-lg" id="id_anio" name="id_anio" >
				<option 0>A&ntilde;o</option>
                 <?php 
                 //AL ARRAY $datos LE ASIGNAMOS TODOS LOS REGISTROS ENCONTRADOS EN LA TABLA 
                 while($datos = mysql_fetch_array($buscar)){ 
                 ?> 
                 
                 <option <?php if($datos["id_anio"] == $aniocond["id_anio"]){echo("selected = selected");}?> value=<?=$datos["id_anio"]?> > <?=$datos["desc_anio"]?> </option> 
                 <?php 
                 } 
                 ?> 
                 <!--CERRAMOS EL COMBOBOX--> 
             </select> 
             
             <?php 
 } 
 ?>
                        </div>
    </div>
						
 <p></p>

                 <!-- <p></p>
                <p></p> -->
                <div class="pull-right">
<!--				<button type="submit" class="btn btn-primary" enabled>Guardar</button>-->
                <input type="image" src="img/Guardar1.png" value="Guardar" />
                
			    <!--<button type="submit" class="btn btn-primary" enabled>Cancelar</button> -->
                <input type="image" src="img/Cancelar1.png" value="Cancelar" />
			  </div>   
             </div>
              
               
			  
              
         
               

<!--</div>-->		
</form>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
     <script src="assets/js/tooltip.js"></script>
	<script src="assets/js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
<script type="text/javascript">
//Funcion ComboBox Otro Perfil
$( document ).ready(function() {
          
	opt = $("#id_perfil_usuario :selected").text();
    if (opt==' Otro ') {
        $('#otroperfil').show();
		 $('#otro').show();
		
    }else {
        $('#otroperfil').hide();
		$('#otro').hide();
		
    }
});
$('#id_perfil_usuario').change(function() {
    
	opt = $("#id_perfil_usuario :selected").text();
    if (opt==' Otro ') {
        $('#otroperfil').show();
		 $('#otro').show();
		
    }else {
        $('#otroperfil').hide();
		$('#otro').hide();
		
    }
});
</script>
</body>
</html>
