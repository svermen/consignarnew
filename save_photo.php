<?php
require_once("includes/initialize.php");	

$upload_errors = array(
UPLOAD_ERR_OK	      => "No hay errores.",
UPLOAD_ERR_INI_SIZE	  => "Más grande que la carga máxima permitida de archivo.",
UPLOAD_ERR_FORM_SIZE  => "Más grande que el tamaño maximo permitido de archivo.",
UPLOAD_ERR_PARTIAL	  => "Carga Parcial.",
UPLOAD_ERR_NO_FILE    => "Ningun archivo.",
UPLOAD_ERR_NO_TMP_DIR => "Sin directorio temporal.",
UPLOAD_ERR_CANT_WRITE => "No se puede escribir en el disco.",
UPLOAD_ERR_EXTENSION  => "Carga de archivos se detuvo por extensión de archivo.",
);



if (isset($_POST['savephoto'])){
	

			
	 $tmp_file = $_FILES['upload_file']['tmp_name'];
	 @$target_file = basename($_FILES['upload_file']['name']);
	 $upload_dir = "uploads";
	 $imgsize = $_FILES['upload_file']['size'];	
	 $imgtype = $_FILES['upload_file']['type'];
	 $member_id = $_SESSION['member_id'];	

	if (move_uploaded_file($tmp_file,$upload_dir."/".$target_file)){
			global $mydb;
			$mydb->setQuery("INSERT INTO `foto`(`filename`, `type`, `size`, `member_id`) 
				VALUES ('{$target_file}', '{$imgtype}', '{$imgsize}', '{$member_id}')");
			$mydb->executeQuery();
			if ($mydb->affected_rows() == 1) {
				
				echo "<script type=\"text/javascript\">
							alert(\"Foto ingresada correctamente.\");
							window.location='perfil33.php';
						</script>";
				
			} else{
				echo "<script type=\"text/javascript\">
							alert(\"Error al ingresar la foto!\");
						</script>";
			}
		
			//echo "File uploaded Succesfully";
			
		}else{
			$error = $_FILES['upload_file']['error'];
			$message = $upload_errors[$error];
			echo "<script type=\"text/javascript\">
							alert(\".{$message}.\");
							
						</script>";
		}
	
}
?>