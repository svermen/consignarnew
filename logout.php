<?php
// Cuatro pasos para el cierre de una sesi�n

// 1. Encuentra la sesion
session_start();

// 2. Destruye todas las variables de sesi�n
$_SESSION = array();

// 3. Destruye la cookie de sesi�n
if(isset($_COOKIE[session_name()])) {
	setcookie(session_name(), '', time()-42000, '/');
}

// 4. Destruye la sesi�n
session_destroy();?>
<script type="text/javascript">
		window.location = "index.php?logout=1";
	</script>
	<?php	
?>